describeThings :: Integer -> String -> String
describeThings 0 word = "no " ++ word ++ "s"
describeThings 1 word = "1 " ++ word
describeThings n word = show n ++ " " ++ word ++ "s"
